package patterns;

import org.openqa.selenium.WebDriver;
import page.BasePageClass;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by Александр on 21.02.2017.
 */
public class ShellHeader extends BasePageClass {

    protected String xpath = ".//header[@id='shell-hdr']";
    /**
     * Возвращает полный xpath кнопки header`a
     * @param dataSapUi
     * @return
     */
    private String getFullXPath(String dataSapUi) {
        return this.xpath + "//a[@data-sap-ui='" + dataSapUi + "']";
    }

    /**
     * Кнопка user
     * @throws InterruptedException
     */
    @Step("User")
    public void me() throws InterruptedException {
        searchElement(getFullXPath("meAreaHeaderButton")).click();
    }

    /**
     * Кнопка back
     * @throws InterruptedException
     */
    @Step("Back")
    public void back() throws InterruptedException {
        searchElement(getFullXPath("backBtn")).click();
    }

    /**
     * Кнопка home
     * @throws InterruptedException
     */
    @Step("Home")
    public void home() throws InterruptedException {
        searchElement(getFullXPath("homeBtn")).click();
    }

    /**
     * Кнопка home
     * @throws InterruptedException
     */
    @Step("Search")
    public void search() throws InterruptedException {
        searchElement(getFullXPath("sf")).click();
    }

    /**
     * Кнопка home
     * @throws InterruptedException
     */
    @Step("App")
    public void app() throws InterruptedException {
        searchElement(getFullXPath("CollaborationToolButton")).click();
    }

    /**
     * Кнопка home
     * @throws InterruptedException
     */
    @Step("Notifications")
    public void notifications() throws InterruptedException {
        searchElement(getFullXPath("NotificationsCountButton")).click();
    }

    /**
     * Поиск приложения
     * @param searchValue
     */
    @Step("Поиск {0}")
    public void executeSearch(String searchValue) throws InterruptedException {
        search(); // Показать форму поиска
        searchElement(this.xpath + "//input[@role='textbox']")
                        .sendKeys(searchValue); // Ввести значение

        searchElement(this.xpath + "//button[@title='Search']")
                        .click(); // Выполнить поиск
    }

    /**
     * Получить название приложения
     */
    @Step("Получить заголовок приложения")
    public String getAppTilte() throws InterruptedException {
        return searchElement(this.xpath + "//span[@class='sapUshellHeadTitle']").getText();
    }

    public ShellHeader(WebDriver driver) {
        super(driver);
    }
}
