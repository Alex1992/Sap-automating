package patterns;

import control.DropDown;
import element.SectionElement;
import element.ViewElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import page.BasePageClass;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by Александр on 22.02.2017.
 */
public class FullScreen extends BasePageClass {


    /** ----------------------------------------- Шапка -------------------------------------- */

    @Step("Применить")
    public void apply() throws InterruptedException {
        getButtonByUIName("Go").click(); // Применить
    }

    @Step("Адаптировать фильтр")
    public void adaptFilter() throws InterruptedException {
        getButtonByDynamicUIName("Фильтр").click(); // Адаптировать фильтр
    }

    @Step("Закрепить")
    public void pin() throws InterruptedException {
        getElemByAbsXPath(".//button[@title='Pin header on press']").click(); // Закрепить
    }

    /** ----------------------------------------- /Шапка -------------------------------------- */



    /** ----------------------------------------- Фильтры -------------------------------------- */

    @Step("Добавление нового фильтра")
    public void addNewFilter(String filterName) throws InterruptedException {

        adaptFilter(); // Адаптировать фильтр

        ViewElement adaptingFilterDialog = new ViewElement(".//div[@role='dialog'][contains(@id,'dialog')]", driver); // Диалог 'Адаптировать фильтр'
        adaptingFilterDialog.getElementByRelXPath("//div/form/input[@type='search']").sendKeys(filterName);
        adaptingFilterDialog.getElementByRelXPath("//div/div/a[contains(text(),'Прочие фильтры')]").click();

        ViewElement selectFilterDialog = new ViewElement(".//div[@role='dialog'][contains(@style,'display: block')]", driver); // Диалог 'Выбор фильтра'
        selectFilterDialog.getElementByRelXPath("//div[label[text()='" + filterName + "']]/preceding-sibling::div//input").click();
        selectFilterDialog.getButtonByRelUIName("ОК");

    }

    /**
     * Поле поиска
     * @return
     * @throws InterruptedException
     */
    public WebElement getSearchField() throws InterruptedException {
        return searchElement(".//header//input[@type='search']");
    }

    /**
     * Возвращает input по UIName (label)
     * @param UIName
     * @return
     * @throws InterruptedException Виды кнопок :
     * "Создать объект" "Настройки"
     */
    @Step("Поле {0}")
    public WebElement getInputByUIName(String UIName) throws InterruptedException {
        return searchElement(".//label[text()='" + UIName + "']/following-sibling::div//input");
    }

    @Step("Установка значения {1} в дропдаун {0}")
    public void dropDownFilterSelectItem(String dropDownFilterUIName, String item) throws InterruptedException {
        WebElement arrowDown = searchElement(".//div[label[text()='" + dropDownFilterUIName + "']]/following-sibling::div//button");
        arrowDown.click();
        try {
            searchElement(".//div[text()='" + item +
                    "']/parent::div/parent::div//preceding-sibling::div//input").click();
        } catch (Exception e) {
            throw new AssertionError("В выпадающем списке нет значения " + item);
        }
        arrowDown.click();
    }

    @Step("Свободный поиск")
    public void freeSearch(String value) throws InterruptedException {
        getSearchField().sendKeys(value);
        getSearchField().submit();
    }

    /** ----------------------------------------- /Фильтры -------------------------------------- */


    /** ----------------------------------------- Таблица -------------------------------------- */

    @Step("Удалить")
    public void delete() throws InterruptedException {
        getButtonByUIName("Удалить").click();
    }

    @Step("Настройки")
    public void settings() throws InterruptedException {
        getPanelButtonByTitle("Настройки").click();
    }

    @Step("Кнопка '+'")
    public void createNewObject() throws InterruptedException {
        getPanelButtonByTitle("Create New Object").click();
    }

    @Step("Кнопка экспорт в таблицу")
    public void exportToSpreadsheet() throws InterruptedException {
        getPanelButtonByTitle("Export to Spreadsheet").click();
    }

    @Step("Проверка существования колонки {0}")
    public Boolean checkColumnExisting(String columnName) throws InterruptedException {
        return getIndexOfColumn(null, columnName) != -1;
    }

    @Step("Больше")
    public void more() throws InterruptedException {
        getElemByAbsXPath(".//li[@role='button'][*/*/*/h1[text()='More']]").click();
    }

    @Step("Всего записей")
    public int totalRecords() throws InterruptedException {
        String val = searchElement(".//div[@role='toolbar']/div[@role='heading']/span").getAttribute("innerHTML");
        return Integer.parseInt(val.substring(val.indexOf("(") + 1, val.indexOf(")")));
    }

    @Step("Добавление колонки {0}")
    public void newColumn(String columnName) throws InterruptedException {
        settings();
        ViewElement dialogFilterBox = new ViewElement(".//div[@role='dialog'][contains(@style,'display: block')]", driver);
        dialogFilterBox.selectTableListItem(columnName);
        dialogFilterBox.getElementByRelXPath("//button/div/span[text()='ОК']").click();
    }

    @Step("Группировка по полю {0}")
    public void groupByField(String fieldName) throws InterruptedException {
        settings();
        ViewElement settingsDialogBox = new ViewElement(".//div[@role='dialog'][contains(@style,'display: block')]", driver);
        settingsDialogBox.getElementByRelXPath("//ul/li[text()='Группировка']").click();
        settingsDialogBox.getElementByRelXPath("//input[@role='combobox']").sendKeys(fieldName); // Выбор поля группировки
        settingsDialogBox.getButtonByRelUIName("ОК");
    }

    @Step("Cортировка по полю {0}")
    public void sortingByField(String fieldName, String sortType) throws InterruptedException {
        settings();
        ViewElement settingsDialogBox = new ViewElement(".//div[@role='dialog'][contains(@style,'display: block')]", driver);
        settingsDialogBox.getElementByRelXPath("//ul/li[text()='Сортировка']").click();
        settingsDialogBox.getElementByRelXPath("//input[@role='combobox']").sendKeys(fieldName); // Выбор поля сортировки
        settingsDialogBox.getElementByRelXPath("//div[@role='combobox']").click(); // Дропдаун типа сортировки
        settingsDialogBox.getElementByRelXPath("//ul[@role='listbox']/li[text()='" + sortType + "']").click(); // Выбор типа сортировки
        settingsDialogBox.getButtonByRelUIName("ОК");
    }

    @Step("Установка значения в фильтр")
    public void setValueInFilter(String filterName, String searchStub, String value) throws InterruptedException {
        getFilterField(filterName).sendKeys(searchStub);
        getFilterPopUpItem(value).click();
    }

    @Step("Проверка сортировки по полю {0}")
    public void checkSortingByField(String field, Boolean ascending) {

        int columnIndex = getIndexOfColumn(null, field);

        int i = 1;
        while (true) {
            try {
                String thisValue = driver.findElement(By.xpath(".//tbody/tr[" + i + "]/td["
                        + columnIndex + "]/span")).getText();
                String nextValue = driver.findElement(By.xpath(".//tbody/tr[" + (i + 1) + "]/td["
                        + columnIndex + "]/span")).getText();

                if ((thisValue.compareTo(nextValue) > 0) == ascending) {

                } else {
                    Assert.fail("Данные отсортированны некорректно!");
                }

            } catch (Exception e) {
                break;
            }
        }

        System.out.println("Данные отсортированны корректно!");
    }

    @Step("Проверка группировки по полю {0}")
    public void checkGroupingByField(String fieldName) {
        // TODO: 17.02.2017 Придумать вариант получше
        try {
            searchElement(".//tr/td/div/span[contains(text()," + fieldName.toUpperCase() + ")]");
        } catch (Exception e) {
            Assert.fail("Группировка провалилась!");
        }

        System.out.println("Группировка прошла успешно!");
    }

    /** ----------------------------------------- /Таблица -------------------------------------- */



    /** ----------------------------------------- Конструкторы контролов --------------------------------------- */

    public DropDown dropDown(String xpath) {
        return new DropDown(driver, xpath);
    }

    public SectionElement getSection(String UIName) { return new SectionElement(".//section[div[div[text()='" + UIName + "']]]", driver); }

    /** ----------------------------------------- /Конструкторы контролов -------------------------------------- */

    public FullScreen(WebDriver driver) {
        super(driver);
    }
}
