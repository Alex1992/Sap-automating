package element;

import control.Control;
import org.openqa.selenium.*;
import patterns.*;

/**
 * Created by Александр on 21.02.2017.
 */
public class SapOUI extends Control {

    protected final WebDriver driver;

    public SapOUI(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    protected SectionElement getSection(String xpath) {
        return new SectionElement(xpath, driver);
    }

    protected ViewElement getView(String xpath) {
        return new ViewElement(xpath, driver);
    }

    protected ShellHeader getHeader() {
        return new ShellHeader(driver);
    }

    protected UserPersonalBar getPersonalBar() {
        return new UserPersonalBar(driver);
    }

    protected AlertDialog getAlertDialog() {
        return new AlertDialog(driver);
    }

    protected FullScreen fullScreenPage() {
        return new FullScreen(driver);
    }

}
