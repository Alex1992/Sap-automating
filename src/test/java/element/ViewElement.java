package element;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import page.BasePageClass;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by Александр on 14.02.2017.
 */

public class ViewElement extends BasePageClass {

    protected String xpath;

    public ViewElement(String xpath, WebDriver driver) {
        super(driver);
        this.xpath = xpath;
    }

    public ViewElement(WebDriver driver) {
        super(driver);
    }

    public WebElement getElementByRelXPath(String relXPath) throws InterruptedException {
        return searchElement(this.xpath + relXPath);
    }

    public WebElement getButtonByRelUIName(String UIName) throws InterruptedException {
        return searchElement(this.xpath + "//button[*[span[text()='" + UIName + "']]]");
    }

    @Step("Выбор значения {0} из списка")
    public void selectTableListItem(String value) throws InterruptedException {
        try {
            getElemByAbsXPath(this.xpath + "//td/span[text()='" + value + "']/parent::td/preceding-sibling::td//input").click();
        } catch (Exception e) {
            throw new AssertionError("В списке нет элемента со значением " + value + " !");
        }
    }


}
