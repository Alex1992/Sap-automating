package element;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import page.BasePageClass;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by Александр on 14.02.2017.
 */

public class SectionElement extends BasePageClass {

    protected String xpath = ".";

    public SectionElement(String xpath, WebDriver driver) {
        super(driver);
        this.xpath = xpath;
    }

    @Step("Переход на третью страницу")
    public void selectRowAndGoThirdPage(String columnName, String value) throws InterruptedException {
        getRowByColumnValue(this.xpath, columnName, value);
    }

    /**
     * Кнопка настройки
     */
    public void settings() {
        searchElement(this.xpath + "//button[@title='Settings']").click();
    }

    public SectionElement(WebDriver driver) {
        super(driver);
    }

    public WebElement getElementByRelXPath(String relXPath) throws InterruptedException {
        return searchElement(this.xpath + relXPath);
    }

    public WebElement getButtonByRelUIName(String UIName) throws InterruptedException {
        return searchElement(this.xpath + "//button[*[span[text()='" + UIName + "']]]");
    }

    @Step("Кнопка панели {0}")
    @Override
    public WebElement getPanelButtonByTitle(String title) throws InterruptedException {
        return searchElement(this.xpath + "//button[@title='" + title + "']");
    }

    @Step("Выбор значения {0} из списка")
    public void selectTableListItem(String value) throws InterruptedException {
        try {
            getElemByAbsXPath(this.xpath + "//td/span[text()='" + value + "']/parent::td/preceding-sibling::td//input").click();
        } catch (Exception e) {
            throw new AssertionError("В списке нет элемента со значением " + value + " !");
        }
    }

    @Step("Получение текста контрола span {0}")
    public String getSpanTextByLabel(String label) throws InterruptedException {
        String value = searchElement(this.xpath + "//div[label[text()='" + label + "']]/following-sibling::div[1]//span").getAttribute("innerHTML");
        System.out.println("Значение :" + value);
        return value;
    }

    @Step("Получение текста контрола ссылки {0}")
    public String getAnchorTextByLabel(String label) throws InterruptedException {
        String value = searchElement(this.xpath + "//div[label[text()='" + label + "']]/following-sibling::div[1]//a").getAttribute("innerHTML");
        System.out.println("Значение :" + value);
        return value;
    }

    @Step("Поиск присутствия значения {1} в столбце {0}")
    public Boolean checkPresenceValueInColumnInSection(String columnName, String value) throws InterruptedException {

        int columnIndex = getIndexOfColumn(this.xpath, columnName);

        try {
            System.out.println("Поиск по xpath :" + this.xpath + "//tbody/tr/td[" + columnIndex + "]//span[text()='" + value + "']");
            driver.findElement(By.xpath(this.xpath + "//tbody/tr/td[" + columnIndex + "]//span[text()='" + value + "']"));
        } catch (Exception e) {
            System.out.println("Значение '" + value + "' столбца '" + columnName + "' не найдено!");
            return false;
        }

        System.out.println("Значение '" + value + "' столбца '" + columnName + "' найдено!");
        return true;
    }

    /**
     * Добавление новой колонки
     * @param columnName
     * @throws InterruptedException
     */
    public void newColumn(String columnName) throws InterruptedException {
        this.settings();
        ViewElement dialogFilterBox = new ViewElement(".//div[@role='dialog'][contains(@style,'display: block')]", driver);
        dialogFilterBox.selectTableListItem(columnName);
        dialogFilterBox.getElementByRelXPath("//button/div/span[text()='ОК']").click();
    }

    @Step("Проверка существования колонки {0}")
    public Boolean checkColumnExisting(String columnName) throws InterruptedException {
        return getIndexOfColumn(this.xpath, columnName) != -1;
    }

    @Step("Группировка по полю {0}")
    public void groupByField(String fieldName) throws InterruptedException {
        this.settings();
        ViewElement settingsDialogBox = new ViewElement(".//div[@role='dialog'][contains(@style,'display: block')]", driver);
        settingsDialogBox.getElementByRelXPath("//ul/li[text()='Группировка']").click();
        settingsDialogBox.getElementByRelXPath("//input[@role='combobox']").sendKeys(fieldName); // Выбор поля группировки
        settingsDialogBox.getButtonByRelUIName("ОК");
    }

    @Step("Cортировка по полю {0}")
    public void sortingByField(String fieldName, String sortType) throws InterruptedException {
        this.settings();
        ViewElement settingsDialogBox = new ViewElement(".//div[@role='dialog'][contains(@style,'display: block')]", driver);
        settingsDialogBox.getElementByRelXPath("//ul/li[text()='Сортировка']").click();
        settingsDialogBox.getElementByRelXPath("//input[@role='combobox']").sendKeys(fieldName); // Выбор поля сортировки
        settingsDialogBox.getElementByRelXPath("//div[@role='combobox']").click(); // Дропдаун типа сортировки
        settingsDialogBox.getElementByRelXPath("//ul[@role='listbox']/li[text()='" + sortType + "']").click(); // Выбор типа сортировки
        settingsDialogBox.getButtonByRelUIName("ОК");
    }

    @Step("Фильтрация по полю {0}")
    public void filterByField(String fieldName, String value) throws InterruptedException {
        this.settings();
        ViewElement settingsDialogBox = new ViewElement(".//div[@role='dialog'][contains(@style,'display: block')]", driver);
        settingsDialogBox.getElementByRelXPath("//ul/li[text()='Фильрация']").click(); // TODO: 24.02.2017 Фильтрация
        settingsDialogBox.getElementByRelXPath("//input[@role='combobox']").sendKeys(fieldName); // Выбор поля сортировки
        settingsDialogBox.getElementByRelXPath("//div[@role='combobox']").click(); // Дропдаун типа сортировки
        settingsDialogBox.getElementByRelXPath("//ul[@role='listbox']/li[text()='" + "equal to" + "']").click(); // Выбор типа сортировки
        settingsDialogBox.getElementByRelXPath("//input[@role='textbox']").sendKeys(value); // Выбор поля сортировки
        settingsDialogBox.getButtonByRelUIName("ОК");
    }

    @Step("Проверка сортировки по полю {0}")
    public void checkSortingByField(String field, Boolean ascending) {

        int columnIndex = getIndexOfColumn(this.xpath, field);

        int i = 1;
        while (true) {
            try {
                String thisValue = driver.findElement(By.xpath(this.xpath + "//tbody/tr[" + i + "]/td["
                        + columnIndex + "]/span")).getText();
                String nextValue = driver.findElement(By.xpath(this.xpath + "//tbody/tr[" + (i + 1) + "]/td["
                        + columnIndex + "]/span")).getText();

                if ((thisValue.compareTo(nextValue) > 0) == ascending) {

                } else {
                    Assert.fail("Данные отсортированны некорректно!");
                }

            } catch (Exception e) {
                break;
            }
        }

        System.out.println("Данные отсортированны корректно!");
    }

    @Step("Проверка группировки по полю {0}")
    public void checkGroupingByField(String fieldName) {
        // TODO: 17.02.2017 Придумать вариант получше
        try {
            searchElement(this.xpath + "//tr/td/div/span[contains(text()," + fieldName.toUpperCase() + ")]");
        } catch (Exception e) {
            Assert.fail("Группировка провалилась!");
        }

        System.out.println("Группировка прошла успешно!");
    }


}
