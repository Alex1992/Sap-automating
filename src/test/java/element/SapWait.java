package element;

import org.openqa.selenium.WebDriver;

/**
 * Created by Александр on 22.02.2017.
 */
public class SapWait {

    private String loadIcon = ".//div[@role='progressbar']";
    private int timeout = 30000;
    protected WebDriver driver;

    public SapWait(WebDriver driver) {
        this.driver = driver;
    }
}
