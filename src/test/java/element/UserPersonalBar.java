package element;

import org.openqa.selenium.WebDriver;
import page.BasePageClass;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by Александр on 21.02.2017.
 */
public class UserPersonalBar extends BasePageClass {

    private String xpath = ".//div[@data-sap-ui='meArea']";

    private String getFullXPath(String dataSapUi) {
        return this.xpath + "//button[@data-sap-ui='" + dataSapUi + "']";
    }

    @Step("App Finder")
    public void appFinder() throws InterruptedException {
        searchElement(getFullXPath("openCatalogBtn")).click();
    }

    @Step("Settings")
    public void settings() throws InterruptedException {
        searchElement(getFullXPath("userSettingsBtn")).click();
    }

    @Step("Edit Home Page")
    public void editHomePage() throws InterruptedException {
        searchElement(getFullXPath("ActionModeBtn")).click();
    }

    @Step("Sign out")
    public void signOut() throws InterruptedException {
        searchElement(getFullXPath("logoutBtn")).click();
    }

    /**
     * Получить полное имя пользователя
     * @return
     * @throws InterruptedException
     */
    public String getFullName() throws InterruptedException {
       return searchElement(this.xpath + "//div/div[@class='sapMFlexItem']/span[contains(@class,'sapMText')]").getText();
    }

    protected UserPersonalBar(WebDriver driver) {
        super(driver);
    }
}
