package element;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.internal.Streams;
import page.BasePageClass;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by Александр on 21.02.2017.
 */
public class AlertDialog extends BasePageClass {

    protected String xpath = ".//div[@role='alertdialog']";

    @Step("Ok")
    public void ok() throws InterruptedException {
        getElemByAbsXPath(this.xpath + "//button//span[text()='OK']").click();
    }

    @Step("Отмена")
    public void cancel() throws InterruptedException {
        getElemByAbsXPath(this.xpath + "//button//span[text()='Cancel']").click();
    }

    protected AlertDialog(WebDriver driver) {
        super(driver);
    }
}

