package test;

import com.sun.javafx.scene.control.skin.TableHeaderRow;
import com.sun.jna.Platform;
import element.SapOUI;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.server.SystemClock;
import org.testng.annotations.*;
import ru.yandex.qatools.allure.annotations.Step;

import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * Created by Александр on 10.02.2017.
 */

public abstract class BaseTestClass {
    public WebDriver driver;

    @BeforeMethod
    public void initDriver() throws Exception {
        DesiredCapabilities capability = DesiredCapabilities.chrome();
        capability.setBrowserName("chrome");
        //capability.setPlatform(Platform.WINDOWS);
        capability.setVersion("3.6");

        URL hostURL = new URL("http://127.0.0.1:4444/wd/hub");
        driver = new RemoteWebDriver(hostURL, capability);
        System.out.println("Окнонание вызова initDriver()");
    }

    @AfterMethod
    public void quitDriver() throws Exception {
        driver.quit();
        System.out.println("Окнонание вызова quitDriver()");
    }


}
