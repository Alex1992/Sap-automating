package test.ShowAndTell1;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import page.*;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.Title;
import test.BaseTestClass;


/**
 * Created by Александр on 20.02.2017.
 */
public class EKDTest extends BaseTestClass {


    @Stories("SAP Megafon")
    @Title("Проверка приложения управления договорами")
    @Features("ЕКД")
    @Description("Проверка единой карточки договора")
    @Test
    public void ekdTest() throws InterruptedException {

        String URL = "http://127.0.0.1:8080/test/flpSandbox.html#Shell-home";
        // Заходим на тестовый стенд
        driver.get(URL);

        /** Переход на страницу выбора предложений */
        BDOMegafonHomeView megafonHomePage = new BDOMegafonHomeView(driver); //
        megafonHomePage.selectApplication("demoproducts"); // Открытие плитки приложения "Управление договорами"

        /** Переход на экран управление договорами */
        BDOEkdMainView ekdMainView = new BDOEkdMainView(driver);
        ekdMainView.checkDataAvailibility("Внутренний номер", "1100004", true); // Проверка наличия договора
        ekdMainView.checkDataAvailibility("Внутренний номер", "1100001", false); // Проверка отсутствия договора

        ekdMainView.checkFreeSearchWorking("1100004"); // Проверка работоспособности свободного поиска TODO: 21.02.2017 Отладить!
        ekdMainView.checkFilter("Контрагент", "связь", "Связь"); // TODO: 21.02.2017 Отладить!
        ekdMainView.checkFilter("Тип договора", "аренды", "Договоры аренды линий связи"); // TODO: 21.02.2017 Отладить!
        ekdMainView.addFilter("Электронная подпись", "1100004"); // Добавление и проверка нового фильтра // TODO: 21.02.2017 Отладить!
        ekdMainView.addColumn("Юридический адрес"); // Добавление новой колонки // TODO: 21.02.2017 Отладить!
        ekdMainView.checkSorting("Внутренний номер", "По восходящей", true); // Проверка  сортировки по полю // TODO: 21.02.2017 Отладить!
        ekdMainView.checkGroupingByField("Тип договора"); // Проверка группировки по полю // TODO: 21.02.2017 Отладить!

        /** Переход на экран детальной информации по договору */
        BDOAgreementDetailInformationView agreementDetailInformationView =
                ekdMainView.goToDetailInformationView("1100004");

        agreementDetailInformationView.checkMainDetail("Договор на закупку комутаторов",
                                                            "1100001"); // Основная информация
        agreementDetailInformationView.checkFilials("Центральный филиал"); // Проверка филиала
        agreementDetailInformationView.checkAgreeSumm("700 000,00"); // Проверка суммы по договору
        agreementDetailInformationView.checkContractor("ООО «КАНЦЛЕР»"); // Проверка контрагента
        agreementDetailInformationView.checkContactData("10006331"); // Проверка контактных данных
        agreementDetailInformationView.checkAgreePayment("70"); // Проверка оплаты по договору
        agreementDetailInformationView.checkСonditionsOnTheCircumstances("18%"); // Проверка условий по обязательствам
        agreementDetailInformationView.checkProccesses("Андрей Анатольевич Герасименко " +
                                                            "(SAPGERASIMEN)"); // Проверкапроцессов
        agreementDetailInformationView.checkDocs("3000001"); // Проверка документов
        agreementDetailInformationView.checkCorpContol("Нет"); // Проверка корпоративного контроля
        agreementDetailInformationView.checkTypeDoc("Закупка ретрансляторов (OB01)"); // Проверка типа предмета договора
        agreementDetailInformationView.checkGroups("000000000322"); // Проверка группы
        agreementDetailInformationView.checkAdministrationData("Алексей Брониславович Дунец " +
                                                                    "(SAPDUNETS)"); // Проверка административных данных

        agreementDetailInformationView.checkSmartLink(); // Проверка работоспособности smartLink

        agreementDetailInformationView.checkContractor(); // Проверка контаргентов

        /** Переход на экран детальной информации по контрагенту */
        BDOContractorDetailView contactorDetailView = agreementDetailInformationView.goToContactorView();
        contactorDetailView.checkGeneralInfo("ООО \"КАНЦЛЕР\""); // Проверка основной информации
        contactorDetailView.checkBankData("0001"); // Проверка банковских данных
        contactorDetailView.checkContractorContacts("Людмила Михайловна Иванова"); // Проверка контактов контрагента
        contactorDetailView.checkAdministrationData("Андрей Брониславович Дунец"); // Проверка административных данных
        contactorDetailView.addColumn("ИдБанковскРеквизитов"); // Проверка добавления колонки

    }

}
