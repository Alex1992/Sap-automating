package test.ShowAndTell1;

import org.testng.annotations.Test;
import page.BDODocumentsMainView;
import page.BDOFullScreenDetailView;
import page.BDOMegafonHomeView;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.Title;
import test.BaseTestClass;


/**
 * Created by Александр on 23.02.2017.
 */
public class ProcessTest extends BaseTestClass {

    @Stories("SAP Megafon")
    @Title("Проверка приложения Управление процессами")
    @Features("Show And Tell 1")
    @Description("Управление процессами")
    @Test
    public void process() throws InterruptedException {

        String URL = "http://127.0.0.1:8080";
        // Заходим на тестовый стенд
        driver.get(URL);

        /** Переход на страницу выбора предложений */
        BDOMegafonHomeView megafonHomePage = new BDOMegafonHomeView(driver);
        megafonHomePage.selectApplication("Управление процессами"); // Открытие плитки приложения "Управление договорами"

        /** Переход на страницу с документами */
        BDODocumentsMainView documentsMainView = new BDODocumentsMainView(driver);
        documentsMainView.checkDataAvailibility("DocumentId", "3000008", true); // Проверка наличия договора
        documentsMainView.checkDataAvailibility("DocumentId", "3000009", false); // Проверка отсутствия договора

        // ---------- проверка фильтров -------------- //
        documentsMainView.checkFreeSearchWorking("3000008");
        documentsMainView.checkFilter("Номер процесса", "3000008", "3000008"); // TODO: !
        documentsMainView.checkFilter("Тип процесса", "Навзвание документа", "Навзвание документа"); // TODO: !
        documentsMainView.checkFilter("Внутренний номер договора", "Автор документа", "Автор документа"); // TODO: !
        documentsMainView.checkFilter("Контрагент", "Дата создания", "Дата создания"); // TODO: !
        documentsMainView.checkFilter("Дата создания процесса", "3000008", "3000008"); // TODO: !
        documentsMainView.checkFilter("Номер активного маршрута процесса", "Навзвание документа", "Навзвание документа"); // TODO: !
        documentsMainView.checkFilter("Статус процесса", "Автор документа", "Автор документа"); // TODO: !
        documentsMainView.checkFilter("Дата завершения процесса", "Автор документа", "Автор документа"); // TODO: !
        documentsMainView.checkFilter("Создатель процесса", "Автор документа", "Автор документа"); // TODO: !
        documentsMainView.checkFilter("Текущий исполнитель", "Автор документа", "Автор документа"); // TODO: !

        // ------------ добавление фильтров --------------//
        documentsMainView.addFilter("Автор изменения", "Документ");
        documentsMainView.addFilter("Дата создания", "Документ");
        documentsMainView.addFilter("Статус отклонения", "Документ");
        documentsMainView.addFilter("Статус документов", "Документ");
        documentsMainView.addFilter("Наименование договора", "Документ");
        documentsMainView.addFilter("Модель", "Документ");

        // ------------ добавление полей --------------//
        documentsMainView.addColumn("Модель");
        documentsMainView.addColumn("Дата создания");
        documentsMainView.addColumn("Дата изменения");
        documentsMainView.addColumn("Автор изменения");
        documentsMainView.addColumn("Название этапа");
        documentsMainView.addColumn("Наименование договора");
        documentsMainView.addColumn("Номер круга согласования");
        documentsMainView.addColumn("Автор");
        documentsMainView.addColumn("Статус отклонения");
        documentsMainView.addColumn("Статус документов");

        documentsMainView.checkSorting("Номер договора", "По восходящей", true); // Проверка  сортировки по полю
        documentsMainView.checkGrouping("Тип процесса"); // Проверка группировки по полю

        // TODO: 11.03.2017 СДЕЛАТЬ СБРОС НАСТРОЕК СОРТИРОВКИ И ГРУППИРОВКИ

        /** Переход на экран детальной информации */
        BDOFullScreenDetailView documentDetailView = documentsMainView.goToDetailView("3000002");
        documentDetailView.checkSectionExisting("Договоры");
        documentDetailView.checkSectionExisting("Документы");
        documentDetailView.checkSectionExisting("Маршруты");
        documentDetailView.checkSectionExisting("Административные данные");

        // Раздел существующие версии документа
        documentDetailView.checkFieldOfTableInSection("Документы", "Номер документа", "3000002");
        documentDetailView.addAndCheckNewColumn("Документы", "Новая колонка"); //
        documentDetailView.checkFieldFilter("Документы", "Номер документа", "3000002");
        documentDetailView.checkSorting("Документы", "Номер документа", "По нисходящей");
        documentDetailView.checkGrouping("Документы", "Номер документа");

        documentDetailView.selectItemAndGoThirdPage("Тип предметов договоров", "Номер документа", "3000002");
        documentDetailView.checkFieldInThirdPage("Основная информация", "данные");
        documentDetailView.checkFieldInThirdPage("Административные данные", "данные");
        documentDetailView.goBackToSecondPage();

        documentDetailView.selectItemAndGoThirdPage("Связанные договоры", "Номер документа", "3000002");
        documentDetailView.checkFieldInThirdPage("Основная информация", "данные");
        documentDetailView.checkFieldInThirdPage("Административные данные", "данные");
        documentDetailView.goBackToSecondPage();

        documentDetailView.selectItemAndGoThirdPage("Контрагенты", "Номер документа", "3000002");
        documentDetailView.checkFieldInThirdPage("Основная информация", "данные");
        documentDetailView.checkFieldInThirdPage("Административные данные", "данные");
        documentDetailView.goBackToSecondPage();


    }

}
