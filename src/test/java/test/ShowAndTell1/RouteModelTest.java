package test.ShowAndTell1;

import org.testng.annotations.Test;
import page.BDODocumentsMainView;
import page.BDOFullScreenDetailView;
import page.BDOMegafonHomeView;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.Title;
import test.BaseTestClass;

/**
 * Created by Александр on 23.02.2017.
 */
public class RouteModelTest extends BaseTestClass {

    @Stories("SAP Megafon")
    @Title("Проверка приложения Модель маршрута")
    @Features("Show And Tell 1")
    @Description("Модель маршрута")
    @Test
    public void routeModel() throws InterruptedException {

        String URL = "http://127.0.0.1:8080";
        // Заходим на тестовый стенд
        driver.get(URL);

        /** Переход на страницу выбора предложений */
        BDOMegafonHomeView megafonHomePage = new BDOMegafonHomeView(driver);
        megafonHomePage.selectApplication("Модель маршрута"); // Открытие плитки приложения "Управление договорами"

        /** Переход на страницу с документами */
        BDODocumentsMainView documentsMainView = new BDODocumentsMainView(driver);
        documentsMainView.checkDataAvailibility("DocumentId", "3000008", true); // Проверка наличия договора
        documentsMainView.checkDataAvailibility("DocumentId", "3000009", false); // Проверка отсутствия договора

        // ---------- проверка фильтров -------------- //
        documentsMainView.checkFreeSearchWorking("3000008");
        documentsMainView.checkFilter("Инициатор модели", "3000008", "3000008"); // TODO: !
        documentsMainView.checkFilter("Статус модели", "Навзвание документа", "Навзвание документа"); // TODO: !
        documentsMainView.checkFilter("ID модели", "Автор документа", "Автор документа"); // TODO: !

        // ------------ добавление фильтров --------------//
        documentsMainView.addFilter("Максимально кругов согласования", "Документ");
        documentsMainView.addFilter("Статус прерывания", "Документ");
        documentsMainView.addFilter("Роль администратора маршрута", "Документ");
        documentsMainView.addFilter("Администратор маршрута", "Документ");
        documentsMainView.addFilter("Автор изменения", "Документ");
        documentsMainView.addFilter("Дата изменения", "Документ");

        // ------------ добавление полей --------------//
        documentsMainView.addColumn("Администратор маршрута");

        documentsMainView.checkSorting("Номер документа", "По восходящей", true); // Проверка  сортировки по полю
        documentsMainView.checkGrouping("Номер документа"); // Проверка группировки по полю

        /** Переход на экран детальной информации */
        BDOFullScreenDetailView documentDetailView = documentsMainView.goToDetailView("3000002");

        // Раздел существующие версии документа
        documentDetailView.checkFieldOfTableInSection("Этапы", "Номер документа", "3000002");
        documentDetailView.addAndCheckNewColumn("Этапы", "Новая колонка"); //
        documentDetailView.checkFieldFilter("Этапы", "Номер документа", "3000002");
        documentDetailView.checkSorting("Этапы", "Номер документа", "По нисходящей");
        documentDetailView.checkGrouping("Этапы", "Номер документа");

        // Раздел существующие версии документа
        documentDetailView.checkFieldOfTableInSection("Обработчик событий", "Номер документа", "3000002");
        documentDetailView.addAndCheckNewColumn("Обработчик событий", "Новая колонка"); //
        documentDetailView.checkFieldFilter("Обработчик событий", "Номер документа", "3000002");
        documentDetailView.checkSorting("Обработчик событий", "Номер документа", "По нисходящей");
        documentDetailView.checkGrouping("Обработчик событий", "Номер документа");

        documentDetailView.selectItemAndGoThirdPage("Задачи", "Номер документа", "3000002");
        documentDetailView.checkFieldInThirdPage("Основная информация", "данные");
        documentDetailView.checkFieldInThirdPage("Административные данные", "данные");
        documentDetailView.goBackToSecondPage();

        // Раздел существующие версии документа
        documentDetailView.checkFieldOfTableInSection("Шаги", "Номер документа", "3000002");
        documentDetailView.addAndCheckNewColumn("Шаги", "Новая колонка"); //
        documentDetailView.checkFieldFilter("Шаги", "Номер документа", "3000002");
        documentDetailView.checkSorting("Шаги", "Номер документа", "По нисходящей");
        documentDetailView.checkGrouping("Шаги", "Номер документа");

        documentDetailView.selectItemAndGoThirdPage("Шаги", "Номер документа", "3000002");
        documentDetailView.checkFieldInThirdPage("Основная информация", "данные");
        documentDetailView.checkFieldInThirdPage("Административные данные", "данные");
        documentDetailView.goBackToSecondPage();

    }

}
