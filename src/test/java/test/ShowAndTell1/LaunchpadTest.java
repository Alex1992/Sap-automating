package test.ShowAndTell1;

import org.testng.annotations.Test;
import page.*;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.Title;
import test.BaseTestClass;

import java.net.MalformedURLException;

/**
 * Created by Александр on 20.02.2017.
 */
public class LaunchpadTest extends BaseTestClass {

    @Stories("SAP Megafon")
    @Title("Проверка launchpad")
    @Features("Launchpad")
    @Description("Проверка launchpad")
    @Test
    public void launchpad() throws InterruptedException {
        String URL = "http://127.0.0.1:8080";
        // Заходим на тестовый стенд
        driver.get(URL);

        /** Переход на страницу выбора приложений */
        BDOLoginPageView loginPageView = new BDOLoginPageView(driver);

        /** Переход на страницу выбора предложений */
        BDOMegafonHomeView megafonHomePage = loginPageView.login("username", "password");
        megafonHomePage.selectApplication("demoproducts");

        /** Переход на экран управление договорами */
        BDOEkdMainView ekdMainView = new BDOEkdMainView(driver);
        ekdMainView.checkFieldAvailibility("Внутренний номер"); // Проверка наличия договора

        /** Переход к результатам поиска */
        BDOSearchResultView searchResultView = ekdMainView.checkAplicationSearch("Управление");
        searchResultView.checkApplicationFound("Управление группами",
                                                true); // Проверка наличия плитки Управление договорами
        searchResultView.checkApplicationFound("Монитор исполнения",
                                                false); // Проверка отсутствия плитки Монитор исполнения

        /** Переход в приложение Управление группами */
        BDOGroupManagementMainView groupManagementMainView = searchResultView.goGroupManagementApp();
        groupManagementMainView.checkAppIsOpened(); // Проверка открытия приложения

        /** Переход обратно на начальную страницу с выбором приложений */
        groupManagementMainView.getMainPage();
        megafonHomePage.editStartPage(); // Вкючение режима редактирование главной страницы
        megafonHomePage.addApplicationInGroup("Управление группами", "My Home"); // Добавление нового приложения
        megafonHomePage.removeApplicationFromGroup("Управление группами", "My Home"); // Удаление приложения
        megafonHomePage.logout(); // Выход из системы

        /** Переход на страницу логина */
        driver.get("http://127.0.0.1:8080"); // Страница логина

        /** Переход на страницу выбора приложений */
        loginPageView.login("username", "password");
        megafonHomePage.selectApplication("Управление маршрутами");

        ekdMainView.checkAplicationSearch("Управление");
        searchResultView.checkApplicationFound("Управление маршрутами",
                true); // Проверка наличия плитки Управление договорами
        searchResultView.checkApplicationFound("Монитор исполнения",
                false); // Проверка отсутствия плитки Монитор исполнения

    }
    
}
