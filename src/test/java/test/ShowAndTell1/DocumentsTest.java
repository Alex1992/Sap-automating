package test.ShowAndTell1;

import org.testng.annotations.Test;
import page.*;
import ru.yandex.qatools.allure.annotations.*;
import test.BaseTestClass;


/**
 * Created by Александр on 23.02.2017.
 */
public class DocumentsTest extends BaseTestClass {

    @Stories("SAP Megafon")
    @Title("Проверка приложения управления документами")
    @Features("Документы")
    @Description("Проверка управления документами")
    @Test
    public void ekdTest() throws InterruptedException {

        String URL = "http://127.0.0.1:8080/test/flpSandbox.html#Shell-home";
        // Заходим на тестовый стенд
        driver.get(URL);

        /** Переход на страницу выбора предложений */
        BDOMegafonHomeView megafonHomePage = new BDOMegafonHomeView(driver);
        megafonHomePage.selectApplication("Управление документами"); // Открытие плитки приложения "Управление договорами"

        /** Переход на страницу с документами */
        BDODocumentsMainView documentsMainView = new BDODocumentsMainView(driver);
        documentsMainView.checkDataAvailibility("DocumentId", "3000008", true); // Проверка наличия договора
        documentsMainView.checkDataAvailibility("DocumentId", "3000009", false); // Проверка отсутствия договора

        documentsMainView.checkFreeSearchWorking("3000008");
        documentsMainView.checkFilter("Номер документа", "3000008", "3000008"); // TODO: !
        documentsMainView.checkFilter("Название документа", "Навзвание документа", "Навзвание документа"); // TODO: !
        documentsMainView.checkFilter("Автор документа", "Автор документа", "Автор документа"); // TODO: !
        documentsMainView.checkFilter("Дата создания", "Дата создания", "Дата создания"); // TODO: !

        documentsMainView.addFilter("Тип документа", "Документ");
        documentsMainView.addFilter("Статус документа", "Документ");
        documentsMainView.addFilter("Версия документа оригинальная или копия", "Документ");
        documentsMainView.addFilter("Версия документа бумажная или цифровая", "Документ");
        documentsMainView.addFilter("Направление документа", "Документ");
        documentsMainView.addFilter("Балансовая единица", "Документ");
        documentsMainView.addFilter("Автор изменения", "Документ");
        documentsMainView.addFilter("Дата изменения", "Документ");

        documentsMainView.addColumn("Статус документа");
        documentsMainView.addColumn("Направление документа");

        documentsMainView.checkSorting("Номер документа", "По восходящей", true); // Проверка  сортировки по полю
        documentsMainView.checkGrouping("Номер документа"); // Проверка группировки по полю

        /** Переход на экран детальной информации */
        BDOFullScreenDetailView documentDetailView = documentsMainView.goToDetailView("3000002");
        documentDetailView.checkSectionExisting("Существующие версии документа");
        documentDetailView.checkSectionExisting("Связанные документы");
        documentDetailView.checkSectionExisting("Существующие виды версий документов");
        documentDetailView.checkSectionExisting("Основная информация для версии документа");
        documentDetailView.checkSectionExisting("Основная информация");
        documentDetailView.checkSectionExisting("Сканирование");
        documentDetailView.checkSectionExisting("Административные данные");
        documentDetailView.checkSectionExisting("Таблица со связанными договорами");
        documentDetailView.checkSectionExisting("Таблица со связанными процессами");

        // Раздел существующие версии документа
        documentDetailView.checkFieldOfTableInSection("Существующие версии документа", "Номер документа", "3000002");
        documentDetailView.addAndCheckNewColumn("Существующие версии документа", "Новая колонка"); //
        documentDetailView.checkFieldFilter("Существующие версии документа", "Номер документа", "3000002");
        documentDetailView.checkSorting("Существующие версии документа", "Номер документа", "По нисходящей");
        documentDetailView.checkGrouping("Существующие версии документа", "Номер документа");

        // Раздел связанные документы
        documentDetailView.checkFieldOfTableInSection("Существующие версии документа", "Номер документа", "3000002");
        documentDetailView.addAndCheckNewColumn("Существующие версии документа", "Новая колонка"); //
        documentDetailView.checkFieldFilter("Существующие версии документа", "Номер документа", "3000002");
        documentDetailView.checkSorting("Существующие версии документа", "Номер документа", "По нисходящей");
        documentDetailView.checkGrouping("Существующие версии документа", "Номер документа");

        // Раздел вид версий
        documentDetailView.checkFieldOfTableInSection("Существующие версии документа", "Номер документа", "3000002");
        documentDetailView.addAndCheckNewColumn("Существующие версии документа", "Новая колонка"); //
        documentDetailView.checkFieldFilter("Существующие версии документа", "Номер документа", "3000002");
        documentDetailView.checkSorting("Существующие версии документа", "Номер документа", "По нисходящей");
        documentDetailView.checkGrouping("Существующие версии документа", "Номер документа");

        // Раздел связанные договоры
        documentDetailView.checkFieldOfTableInSection("Существующие версии документа", "Номер документа", "3000002");
        documentDetailView.addAndCheckNewColumn("Существующие версии документа", "Новая колонка"); //
        documentDetailView.checkFieldFilter("Существующие версии документа", "Номер документа", "3000002");
        documentDetailView.checkSorting("Существующие версии документа", "Номер документа", "По нисходящей");
        documentDetailView.checkGrouping("Существующие версии документа", "Номер документа");

        // Раздел связанные процессы
        documentDetailView.checkFieldOfTableInSection("Существующие версии документа", "Номер документа", "3000002");
        documentDetailView.addAndCheckNewColumn("Существующие версии документа", "Новая колонка"); //
        documentDetailView.checkFieldFilter("Существующие версии документа", "Номер документа", "3000002");
        documentDetailView.checkSorting("Существующие версии документа", "Номер документа", "По нисходящей");
        documentDetailView.checkGrouping("Существующие версии документа", "Номер документа");

        documentDetailView.selectItemAndGoThirdPage("Существующие версии документа", "Номер документа", "3000002");
        documentDetailView.checkFieldInThirdPage("Основная информация", "данные");
        documentDetailView.checkFieldInThirdPage("Административные данные", "данные");
        documentDetailView.goBackToSecondPage();

        documentDetailView.selectItemAndGoThirdPage("Существующие версии документа", "Номер документа", "3000002");
        documentDetailView.checkFieldInThirdPage("Основная информация", "данные");
        documentDetailView.checkFieldInThirdPage("Административные данные", "данные");
        documentDetailView.goBackToSecondPage();

        documentDetailView.selectItemAndGoThirdPage("Существующие версии документа", "Номер документа", "3000002");
        documentDetailView.checkFieldInThirdPage("Основная информация", "данные");
        documentDetailView.checkFieldInThirdPage("Административные данные", "данные");
        documentDetailView.goBackToSecondPage();

        documentDetailView.selectItemAndGoThirdPage("Существующие версии документа", "Номер документа", "3000002");
        documentDetailView.checkFieldInThirdPage("Основная информация", "данные");
        documentDetailView.checkFieldInThirdPage("Административные данные", "данные");
        documentDetailView.goBackToSecondPage();

        documentDetailView.selectItemAndGoThirdPage("Существующие версии документа", "Номер документа", "3000002");
        documentDetailView.checkFieldInThirdPage("Основная информация", "данные");
        documentDetailView.checkFieldInThirdPage("Административные данные", "данные");
        documentDetailView.goBackToSecondPage();






    }

}
