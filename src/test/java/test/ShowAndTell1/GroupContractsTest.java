package test.ShowAndTell1;

import org.testng.annotations.Test;
import page.BDODocumentsMainView;
import page.BDOFullScreenDetailView;
import page.BDOMegafonHomeView;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.Title;
import test.BaseTestClass;


/**
 * Created by Александр on 23.02.2017.
 */
public class GroupContractsTest extends BaseTestClass {

    @Stories("SAP Megafon")
    @Title("Проверка приложения Группа договоров")
    @Features("Show And Tell 1")
    @Description("Группа договоров\"")
    @Test
    public void gropContracts() throws InterruptedException {

        String URL = "http://127.0.0.1:8080";
        // Заходим на тестовый стенд
        driver.get(URL);

        /** Переход на страницу выбора предложений */
        BDOMegafonHomeView megafonHomePage = new BDOMegafonHomeView(driver);
        megafonHomePage.selectApplication("Управление группами договоров"); // Открытие плитки приложения "Управление договорами"

        /** Переход на страницу с документами */
        BDODocumentsMainView documentsMainView = new BDODocumentsMainView(driver);
        documentsMainView.checkDataAvailibility("DocumentId", "3000008", true); // Проверка наличия договора
        documentsMainView.checkDataAvailibility("DocumentId", "3000009", false); // Проверка отсутствия договора

        // ---------- проверка фильтров -------------- //
        documentsMainView.checkFreeSearchWorking("3000008");
        documentsMainView.checkFilter("Номер и название группы", "3000008", "3000008"); // TODO: !
        documentsMainView.checkFilter("Номер балансовой единицы", "Навзвание документа", "Навзвание документа"); // TODO: !
        documentsMainView.checkFilter("Номер контрагента", "Автор документа", "Автор документа"); // TODO: !
        documentsMainView.checkFilter("Остаток лимита", "Дата создания", "Дата создания"); // TODO: !
        documentsMainView.checkFilter("Дата начала действия", "3000008", "3000008"); // TODO: !
        documentsMainView.checkFilter("Дата конца действия", "Навзвание документа", "Навзвание документа"); // TODO: !
        documentsMainView.checkFilter("Автор документа", "Автор документа", "Автор документа"); // TODO: !

        // ------------ добавление фильтров --------------//
        documentsMainView.addFilter("Название группы договоров", "Документ");
        documentsMainView.addFilter("Решение группы договоров", "Документ");
        documentsMainView.addFilter("Дата решения", "Документ");
        documentsMainView.addFilter("Ответственный", "Документ");
        documentsMainView.addFilter("Статус группы договоров", "Документ");
        documentsMainView.addFilter("Тип группы договоров", "Документ");
        documentsMainView.addFilter("Сумма лимита", "Документ");
        documentsMainView.addFilter("Лимит  в валюте БЕ", "Документ");
        documentsMainView.addFilter("Потребляемый лимит", "Документ");
        documentsMainView.addFilter("Дата создания", "Документ");
        documentsMainView.addFilter("Дата изменения", "Документ");
        documentsMainView.addFilter("Создал", "Документ");
        documentsMainView.addFilter("Изменил", "Документ");

        // ------------ добавление полей --------------//
        documentsMainView.addColumn("Валюта");
        documentsMainView.addColumn("Валюта БЕ");
        documentsMainView.addColumn("Дата изменения");
        documentsMainView.addColumn("Дата решения");
        documentsMainView.addColumn("Дата создания");
        documentsMainView.addColumn("Изменил");
        documentsMainView.addColumn("Лимит в валюте БЕ");
        documentsMainView.addColumn("Название");
        documentsMainView.addColumn("Ответственный");
        documentsMainView.addColumn("Потребляемый лимит");
        documentsMainView.addColumn("Решение группы договоров");
        documentsMainView.addColumn("Создал");
        documentsMainView.addColumn("Сумма лимита");
        documentsMainView.addColumn("Статус группы договоров");
        documentsMainView.addColumn("Тип группы договоров");


        documentsMainView.checkSorting("Номер документа", "По восходящей", true); // Проверка  сортировки по полю
        documentsMainView.checkGrouping("Номер документа"); // Проверка группировки по полю

        /** Переход на экран детальной информации */
        BDOFullScreenDetailView documentDetailView = documentsMainView.goToDetailView("3000002");
        documentDetailView.checkSectionExisting("Существующие типы предметов договоров");
        documentDetailView.checkSectionExisting("Связанные договоры");
        documentDetailView.checkSectionExisting("Существующие контакты");
        documentDetailView.checkSectionExisting("Основная информация для группы договоров");
        documentDetailView.checkSectionExisting("Основная информация");
        documentDetailView.checkSectionExisting("Лимит");
        documentDetailView.checkSectionExisting("Административные данные");

        // Раздел существующие версии документа
        documentDetailView.checkFieldOfTableInSection("Детализация группы", "Номер документа", "3000002");
        documentDetailView.addAndCheckNewColumn("Детализация группы", "Новая колонка"); //
        documentDetailView.checkFieldFilter("Детализация группы", "Номер документа", "3000002");
        documentDetailView.checkSorting("Детализация группы", "Номер документа", "По нисходящей");
        documentDetailView.checkGrouping("Детализация группы", "Номер документа");

        documentDetailView.selectItemAndGoThirdPage("Тип предметов договоров", "Номер документа", "3000002");
        documentDetailView.checkFieldInThirdPage("Основная информация", "данные");
        documentDetailView.checkFieldInThirdPage("Административные данные", "данные");
        documentDetailView.goBackToSecondPage();

        documentDetailView.selectItemAndGoThirdPage("Связанные договоры", "Номер документа", "3000002");
        documentDetailView.checkFieldInThirdPage("Основная информация", "данные");
        documentDetailView.checkFieldInThirdPage("Административные данные", "данные");
        documentDetailView.goBackToSecondPage();

        documentDetailView.selectItemAndGoThirdPage("Контрагенты", "Номер документа", "3000002");
        documentDetailView.checkFieldInThirdPage("Основная информация", "данные");
        documentDetailView.checkFieldInThirdPage("Административные данные", "данные");
        documentDetailView.goBackToSecondPage();







    }

}
