package page;

import org.bouncycastle.jcajce.provider.symmetric.ARC4;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by Александр on 16.02.2017.
 */
public class BDOMegafonHomeView extends BasePageClass {

    public BDOMegafonHomeView(WebDriver driver) {
        super(driver);
    }

    @Step("Выбор приложенеия {0}")
    public void selectApplication(String appName) throws InterruptedException {
        getElemByAbsXPath(".//div[div[div[span[span[text()='" + appName + "']]]]]").click();
        Thread.sleep(5000);
    }

    @Step("Закрыть диалоговое окно с ошибкой")
    public void closeErrorWindow() throws InterruptedException {
        getAlertDialog().ok();
    }

    public void test() throws InterruptedException {
        getAlertDialog().ok();
        getAlertDialog().cancel();
    }


    @Step("Редактирование начальной страницы")
    public void editStartPage() throws InterruptedException {
        getHeader().me(); // Кнопка пользователь

        getPersonalBar().editHomePage();
    }

    @Step("Добавление приложения {0} к группе {1}")
    public void addApplicationInGroup(String appName, String groupName) throws InterruptedException {
        newTile(groupName);
        addApp(appName);
    }

    @Step("Удаление приложения {0} из группы {1}")
    public void removeApplicationFromGroup(String appName, String groupName) throws InterruptedException {
        getElemByAbsXPath(".//div[div[div[div[div[h2[text()='" + groupName + "']]]]]]//li[*/*/*/*/*/*/span[contains(text(),'" + appName + "')]]//div[span[@title='Remove']]").click();
    }

    @Step("Выход из приложения")
    public void logout() throws InterruptedException {
        getPersonalBar().signOut();
        getAlertDialog().ok();
    }

    /** ================================= Общие методы =================================== */

    public void newTile(String groupName) throws InterruptedException {
        getElemByAbsXPath(".//div[div[div[div[div[h2[text()='" + groupName + "']]]]]]//div[span[@aria-label='openAppFinder']]").click();
    }


    public void addApp(String appName) throws InterruptedException {
        getElemByAbsXPath(".//div[@data-sap-ui='appFinderPage']//li[*/*/*/*/*/*/span[contains(text(),'" + appName + "')]]/footer/button").click();
    }



}
