package page;

import element.SectionElement;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import patterns.FullScreen;
import patterns.ShellHeader;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by Александр on 24.02.2017.
 */
public class BDOFullScreenDetailView extends BasePageClass {

    private FullScreen screen = new FullScreen(driver);
    private ShellHeader header = new ShellHeader(driver);

    @Step("Проверка наличия раздела {0}")
    public void checkSectionExisting(String sectionName) {
        try {
            getElemByAbsXPath(".//section[div[div[text()='" + sectionName + "']]]");
        } catch (Exception e) {
            Assert.fail("Раздел " + sectionName + " не найден!");
        }
    }

    @Step("Выбрать запись и перейти на третью страницу")
    public void selectItemAndGoThirdPage(String sectionName, String columnName, String value) throws InterruptedException {
        screen.getSection(sectionName).selectRowAndGoThirdPage(columnName, value);
    }

    @Step("Переход назад")
    public void goBackToSecondPage() throws InterruptedException {
        header.back();
    }

    @Step("Проверка поля {0}")
    public void checkFieldInThirdPage(String fieldName, String fieldValue) throws InterruptedException {
        Assert.assertEquals(new SectionElement(driver).getSpanTextByLabel(fieldName), fieldValue, assertErrorMessage);
    }

    @Step("Добавление и проверка новой колонки")
    public void addAndCheckNewColumn(String sectionName, String columnName) throws InterruptedException {
        SectionElement section = screen.getSection(sectionName);
        section.newColumn(columnName);
        section.checkColumnExisting(columnName);
    }

    @Step("Проверка фильтрации полей ")
    public void checkFieldFilter(String sectionName, String filterName, String value) throws InterruptedException {
        SectionElement section = screen.getSection(sectionName);
        section.filterByField(filterName, value);
        section.checkPresenceValueInColumnInSection(filterName, value);
    }

    @Step("Проверка сортировки")
    public void checkSorting(String sectionName, String fieldName, String sortType) throws InterruptedException {
        SectionElement section = screen.getSection(sectionName);
        section.sortingByField(fieldName, sortType);
        section.checkSortingByField(fieldName, true);
    }

    @Step("Проверка группировки")
    public void checkGrouping(String sectionName, String fieldName) throws InterruptedException {
        SectionElement section = screen.getSection(sectionName);
        section.groupByField(fieldName);
        section.checkGroupingByField(fieldName);
    }

    @Step("Проверка поля {1} в разделе {0}")
    public void checkFieldInSection(String sectionName, String fieldName, String fieldValue) throws InterruptedException {
        Assert.assertEquals(screen.getSection(sectionName).getSpanTextByLabel(fieldName), fieldValue, assertErrorMessage);
    }

    @Step("Проверка поля колонки {1} в разделе {0}")
    public void checkFieldOfTableInSection(String sectionName, String columnName, String columnValue) throws InterruptedException {
        Assert.assertTrue(screen.getSection(sectionName).checkPresenceValueInColumnInSection(columnName, columnValue), assertErrorMessage);
    }


    public BDOFullScreenDetailView(WebDriver driver) {
        super(driver);
    }
}
