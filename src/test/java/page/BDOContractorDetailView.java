package page;

import element.SectionElement;
import element.ViewElement;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by Александр on 20.02.2017.
 */
public class BDOContractorDetailView extends BasePageClass {

    public void test() throws InterruptedException {

    }

    @Step("Проверка общей информации")
    public void checkGeneralInfo(String contactror) throws InterruptedException {
        // TODO: 20.02.2017 Секция не определяется однозначно
        SectionElement mock = getSection(".");
        Assert.assertEquals(mock.getSpanTextByLabel("Country Key"), contactror, assertErrorMessage);
    }

    @Step("Проверка банковских данных")
    public void checkBankData(String value) throws InterruptedException {
        SectionElement mainInfo = new SectionElement(".//section[div[div[text()='Банковские данные']]]", driver);
        Assert.assertTrue(mainInfo.checkPresenceValueInColumnInSection("ИдБанковскихРеквизитов", value), assertErrorMessage);
    }

    @Step("Проверка банковских данных")
    public void checkContractorContacts(String value) throws InterruptedException {
        SectionElement mainInfo = new SectionElement(".//section[div[div[text()='Контакты контрагента']]]", driver);
        Assert.assertTrue(mainInfo.checkPresenceValueInColumnInSection("Полное имя", value), assertErrorMessage);
    }

    @Step("Проверка аминистративных данных")
    public void checkAdministrationData(String withInterest) throws InterruptedException {
        SectionElement mainInfo = new SectionElement(".//section[div[div[text()='Административные данные']]]", driver);
        Assert.assertEquals(mainInfo.getSpanTextByLabel("Создал"), withInterest, assertErrorMessage);
    }

    @Step("Добавление колонки в таблицу с банковскими реквизитами")
    public void addColumn(String columnName) throws InterruptedException {

        SectionElement bankData = new SectionElement(".//section[div[div[text()='Банковские данные']]]", driver);
        bankData.getPanelButtonByTitle("Настройки").click();
        ViewElement dialogFilterBox = new ViewElement(".//div[@role='dialog'][contains(@style,'display: block')]", driver);

        dialogFilterBox.selectTableListItem(columnName);
        dialogFilterBox.getElementByRelXPath("//button/div/span[text()='ОК']").click();
    }

    public BDOContractorDetailView(WebDriver driver) {
        super(driver);
    }
}
