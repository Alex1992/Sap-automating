package page;

import element.SectionElement;
import element.ViewElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import patterns.FullScreen;
import patterns.ShellHeader;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by Александр on 17.02.2017.
 */
public class BDOAgreementDetailInformationView extends BasePageClass {

    protected FullScreen screen = new FullScreen(driver);
    protected ShellHeader header = new ShellHeader(driver);


    @Step("Проверка основной информации")
    public void checkMainDetail(String agreeName, String agreeNumber) throws InterruptedException {
        SectionElement mainInfo = new SectionElement(".", driver); // TODO: 20.02.2017 Костыльная секция
        Assert.assertEquals(mainInfo.getSpanTextByLabel("Наименование"), agreeName, assertErrorMessage);
        Assert.assertEquals(mainInfo.getAnchorTextByLabel("Внутренний номер"), agreeNumber, assertErrorMessage);
    }

    @Step("Проверка филиалов")
    public void checkFilials(String filialName) throws InterruptedException {
        SectionElement mainInfo = new SectionElement(".//section[div[div[text()='Филиалы']]]", driver);
        Assert.assertTrue(mainInfo.checkPresenceValueInColumnInSection("Полное название", filialName), assertErrorMessage);
    }

    @Step("Проверка суммы по договору")
    public void checkAgreeSumm(String summValue) throws InterruptedException {
        SectionElement mainInfo = new SectionElement(".//section[div[div[text()='Сумма по договору ']]]", driver);
        System.out.println(getElemByAbsXPath(".//section[div[div[text()='Сумма по договору ']]]//tbody/tr/td[1]//span[contains(text(),'700')]").getText());
        Assert.assertEquals(getElemByAbsXPath(".//section[div[div[text()='Сумма по договору ']]]//tbody/tr/td[1]//span[contains(text(),'700')]").getText(),
                                    summValue, assertErrorMessage); // TODO: 20.02.2017 Костылище
    }


    @Step("Проверка контрагента")
    public void checkContractor(String contractorName) throws InterruptedException {
        SectionElement mainInfo = new SectionElement(".//section[div[div[text()='Контрагенты']]]", driver);
        Assert.assertTrue(mainInfo.checkPresenceValueInColumnInSection("Наименование контрагента", contractorName), assertErrorMessage);
    }


    @Step("Проверка контактных данных")
    public void checkContactData(String contactData) throws InterruptedException {
        SectionElement mainInfo = new SectionElement(".//section[div[div[text()='Подписанты']]]", driver);
        Assert.assertTrue(mainInfo.checkPresenceValueInColumnInSection("Доверенность", contactData), assertErrorMessage);
    }

    @Step("Проверка оплаты по договору")
    public void checkAgreePayment(String contactData) throws InterruptedException {
        SectionElement mainInfo = new SectionElement(".//section[div[div[text()='Оплата по договору']]]", driver);
        Assert.assertTrue(mainInfo.checkPresenceValueInColumnInSection("% оплаты", contactData), assertErrorMessage);
    }

    @Step("Проверка условий по обстоятельствам")
    public void checkСonditionsOnTheCircumstances(String ndsRate) throws InterruptedException {
        SectionElement mainInfo = new SectionElement(".//section[div[div[text()='Условия по обязательствам']]]", driver);
        Assert.assertEquals(mainInfo.getSpanTextByLabel("Ставка НДС"), ndsRate, assertErrorMessage);
    }

    @Step("Проверка процессов")
    public void checkProccesses(String creator) throws InterruptedException {
        SectionElement mainInfo = new SectionElement(".//section[div[div[text()='Процессы']]]", driver);
        Assert.assertTrue(mainInfo.checkPresenceValueInColumnInSection("Создал", creator), assertErrorMessage);
    }

    @Step("Проверка документов")
    public void checkDocs(String doc) throws InterruptedException {
        SectionElement mainInfo = new SectionElement(".//section[div[div[text()='Документы']]]", driver);
        Assert.assertEquals(getElemByAbsXPath(".//section[div[div[text()='Документы']]]//tbody/tr//span[text()='3000001']").getText(),
                doc, assertErrorMessage); // TODO: 20.02.2017 Костылище
    }

    @Step("Проверка корпоративного контроля")
    public void checkCorpContol(String withInterest) throws InterruptedException {
        SectionElement mainInfo = new SectionElement(".//section[div[div[text()='Корпоративные контроли']]]", driver);
        Assert.assertEquals(mainInfo.getSpanTextByLabel("Сделка с заинтересованностью"), withInterest, assertErrorMessage);
    }

    @Step("Проверка типа предмета договора")
    public void checkTypeDoc(String type) throws InterruptedException {
        SectionElement mainInfo = new SectionElement(".//section[div[div[text()='Тип предмета договора']]]", driver);
        Assert.assertTrue(mainInfo.checkPresenceValueInColumnInSection("Предмет договора", type), assertErrorMessage);
    }

    @Step("Проверка группы")
    public void checkGroups(String groupType) throws InterruptedException {
        SectionElement mainInfo = new SectionElement(".//section[div[div[text()='Группы']]]", driver);
        Assert.assertTrue(mainInfo.checkPresenceValueInColumnInSection("Группа", groupType), assertErrorMessage);
    }

    @Step("Проверка аминистративных данных")
    public void checkAdministrationData(String withInterest) throws InterruptedException {
        SectionElement mainInfo = new SectionElement(".//section[div[div[text()='Административные данные']]]", driver);
        Assert.assertEquals(mainInfo.getSpanTextByLabel("Создал"), withInterest, assertErrorMessage);
    }

    @Step("Проверка работоспособности smartLink")
    public void checkSmartLink() throws InterruptedException {
        SectionElement processes = new SectionElement(".//section[div[div[text()='Процессы']]]", driver);
        processes.getElementByRelXPath("//tbody//tr").click(); // Провалиться процессы

        Assert.assertEquals(getElemByAbsXPath(".//div[@class='sapUshellShellHeadCenter']//div/div/span"),
                                                        "Процесс", assertErrorMessage);

        header.back(); // Вернуться назад
    }

    @Step("Проверка добавления поля 'Наименование контрагента'")
    public void checkContractor() throws InterruptedException {
        SectionElement contractors = new SectionElement(".//section[div[div[text()='Контрагенты']]]", driver);
        Assert.assertEquals(contractors.checkPresenceValueInColumnInSection("Наименование контрагента", "ООО \"Канцлер\""),  assertErrorMessage);
    }

    @Step("Переход на детализацию контрагентов")
    public BDOContractorDetailView goToContactorView() throws InterruptedException {
        SectionElement contractors = new SectionElement(".//section[div[div[text()='Контрагенты']]]", driver);
        contractors.getElementByRelXPath("//tbody//tr").click(); // Провалиться в контрагента
        return new BDOContractorDetailView(driver);
    }


    public BDOAgreementDetailInformationView(WebDriver driver) {
        super(driver);
    }
}
