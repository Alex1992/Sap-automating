package page;

import element.ViewElement;
import org.omg.CORBA.BooleanHolder;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by Александр on 16.02.2017.
 */
public class BDOSearchResultView extends BasePageClass {

    /**
     * Получить плитку с приложением
     * @param appName
     */
    private WebElement getAppTile(String appName) throws InterruptedException {
        return searchElement(".//div[@title='Launch tile'][*/div[contains(@aria-label,'" + appName + "')][@role='button']]");
    }

    @Step("Проверка наличия/отсутствия плитки приложения {0}")
    public void checkApplicationFound(String appName, Boolean availibility) throws InterruptedException {
        if(availibility) {
            try {
                driver.findElement(By.xpath(".//div[@title='Launch tile'][*/div[contains(@aria-label,'" + appName + "')][@role='button']]"));
            } catch (Exception e) {
                throw new AssertionError("Плитка " + appName + " не найдена!");
            }
            System.out.println("Плитка " + appName + " найдена");
        } else {
            try {
                driver.findElement(By.xpath(".//div[@title='Launch tile'][*/div[contains(@aria-label,'" + appName + "')][@role='button']]"));
            } catch (Exception e) {
                System.out.println("Плитка " + appName + " не найдена");
                return;
            }
            throw new AssertionError("Плитка " + appName + " найдена, хотя её быть не должно!");
        }
    }

    @Step("Переход на страницу 'Управление группами'")
    public BDOGroupManagementMainView goGroupManagementApp() throws InterruptedException {
        getAppTile("Управление группами").click();
        return new BDOGroupManagementMainView(driver);
    }


    public BDOSearchResultView(WebDriver driver) {
        super(driver);
    }
}
