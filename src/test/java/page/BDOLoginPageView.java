package page;

import org.openqa.selenium.WebDriver;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by Александр on 21.02.2017.
 */
public class BDOLoginPageView extends BasePageClass {

    @Step("Вход")
    public BDOMegafonHomeView login(String username, String password) throws InterruptedException {

        getElemByAbsXPath(".//input[@name='sap-user']").sendKeys(username);
        getElemByAbsXPath(".//input[@name='sap-password']").sendKeys(password);
        getElemByAbsXPath(".//button[@type='submit']").click();

        return new BDOMegafonHomeView(driver);
    }


    public BDOLoginPageView(WebDriver driver) {
        super(driver);
    }
}
