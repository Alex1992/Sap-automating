package page;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by Александр on 21.02.2017.
 */
public class BDOGroupManagementMainView extends BasePageClass {

    @Step("Проверка, что приложение открылось")
    public void checkAppIsOpened() throws InterruptedException {
        Assert.assertEquals(getHeader().getAppTilte(), "Управление договорами");
    }

    @Step("Переход 'Домой'")
    public void getMainPage() throws InterruptedException {
        getHeader().home();
    }

    protected BDOGroupManagementMainView(WebDriver driver) {
        super(driver);
    }
}
