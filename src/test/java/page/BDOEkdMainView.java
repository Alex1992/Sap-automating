package page;

import element.ViewElement;
import org.apache.commons.beanutils.PropertyUtilsBean;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import patterns.FullScreen;
import patterns.ShellHeader;
import ru.yandex.qatools.allure.annotations.Step;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Александр on 16.02.2017.
 */
public class BDOEkdMainView extends BasePageClass {

    private ShellHeader header = new ShellHeader(driver);
    private FullScreen screen = new FullScreen(driver);

    public void test() throws InterruptedException {
        screen.addNewFilter("Currency");
    }

    public static void main(String args[]) {

        System.out.println();
    }

    @Step("Провалиться в договор")
    public BDOAgreementDetailInformationView goToDetailInformationView(String agreementNumber) throws InterruptedException {

        getFilterField("Внутренний номер").sendKeys(agreementNumber);
        getFilterField("Внутренний номер").submit();
        screen.getInputByUIName("Применить").click();

        getElemByAbsXPath(".//table/tbody/tr/span[text()='" + agreementNumber + "']").click();

        return new BDOAgreementDetailInformationView(driver);
    }

    @Step("Проверка поиска по фильтру {0}")
    public void checkFilter(String filterName, String searchStub, String value) throws InterruptedException {
        screen.setValueInFilter(filterName, searchStub, value);
        screen.apply();
        Assert.assertTrue(checkPresenceValueInColumn(filterName, value)); // Проверяем, что вывелось хоть 1 значение
    }


    @Step("Проверка сортировки по полю {0}")
    public void checkSorting(String field, String sortType, Boolean ascending) throws InterruptedException {

        screen.sortingByField(field, sortType);
        screen.checkSortingByField(field, ascending);
    }

    @Step("Проверка группировки по полю {0}")
    public void checkGroupingByField(String fieldName) throws InterruptedException {
        screen.groupByField(fieldName);
        screen.checkGroupingByField(fieldName);
    }

    @Step("Проверка поиска по фильтру {0}")
    public void checkDropDownFilter(String filterName, String value) throws InterruptedException {

        screen.dropDownFilterSelectItem(filterName, value);

        screen.getInputByUIName("Применить").click();

        Assert.assertTrue(checkPresenceValueInColumn(filterName, value)); // Проверяем, что вывелось хоть 1 значение
        // TODO: 16.02.2017 Поправить

    }

    @Step("Проверка данных столбца {0} значения {1}")
    public void checkDataAvailibility(String columnName, String value, Boolean availibility) throws InterruptedException {
        screen.apply();
        if(availibility)
            Assert.assertTrue(checkPresenceValueInColumn(columnName, value)); // Проверка наличия договора
        else
            Assert.assertFalse(checkPresenceValueInColumn(columnName, value)); // Проверка отсутствия договора

    }

    @Step("Добавление колонки {0}")
    public void addColumn(String columnName) throws InterruptedException {
        screen.newColumn(columnName);
    }

    @Step("Добавление и проверка нового фильтра")
    public void addFilter(String filterName, String value) throws InterruptedException {

        screen.addNewFilter(filterName);

        try {
            getFilterField(filterName);
        } catch (Exception e) {
            Assert.fail("Добавление фильтра провалено!");
        }

        checkDropDownFilter(filterName, value);

    }

    @Step("Проверка работоспособности свободного поиска")
    public void checkFreeSearchWorking(String value) throws InterruptedException {
        screen.freeSearch(value); // Свободный поиск по ID и наименованию
        Assert.assertTrue(checkPresenceValueInColumn("Внутренний номер", value)); // Проверка наличия договора
    }

    @Step("Проверка доступности поля")
    public void checkFieldAvailibility(String fieldName) throws InterruptedException {
        getFilterField(fieldName).isEnabled();
    }

    @Step("Поиск по '{0}', переход к результатам поиска")
    public BDOSearchResultView checkAplicationSearch(String searchStub) throws InterruptedException {

        header.executeSearch(searchStub); // Ищем

        return new BDOSearchResultView(driver);
    }


    public BDOEkdMainView(WebDriver driver) {
        super(driver);
    }
}
