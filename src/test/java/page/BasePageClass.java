package page;

import element.SapOUI;
import element.ViewElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by Александр on 13.02.2017.
 */
public abstract class BasePageClass extends SapOUI {

    protected final WebDriver driver;
    protected String assertErrorMessage = "Фактический результат не соответствует ожидаемому!";

    protected BasePageClass(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }



    /**
     * Метод возвращает новый экземпляр класса viewElement
     * @param xpath
     * @return
     */
    public ViewElement viewElement(String xpath) {
        return new ViewElement(xpath, this.driver);
    }


    /*****************************************************************************************************************************/
    /******************************************** ВСЕ ИСПОЛЬЗУЕМЫЕ ВЕБ ЭЛЕМЕНТЫ **************************************************/
    /*****************************************************************************************************************************/



    /** Контролы ----------------------------------------------------------------------------------------------------------------*/

    @Step("Кнопка {0}")
    public WebElement getButtonByUIName(String UIName) throws InterruptedException {
        return searchElement(".//button[*[span[text()='" + UIName + "']]]");
    }

    @Step("Кнопка с динамическим названием {0}")
    public WebElement getButtonByDynamicUIName(String dinamicUIName) throws InterruptedException {
        return searchElement(".//button[div[span[contains(text(), '" + dinamicUIName + "')]]]");
    }

    @Step("Кнопка панели {0}")
    public WebElement getPanelButtonByTitle(String title) throws InterruptedException {
        return searchElement(".//button[@title='" + title + "']");
    }

    @Step("Поле фильтра {0}")
    public WebElement getFilterField(String fieldName) throws InterruptedException {
        return searchElement(".//[label[text()='" + fieldName +
                                "']]/following-sibling::div/div/div/div/following-sibling::div/input");
    }

    @Step("Попап фильтра")
    public WebElement getFilterPopUpItem(String item) throws InterruptedException {
        return searchElement(".//div[contains(@id, 'popup')]//span[contains(text(), '" + item + "')]");
    }

    /** Списки ----------------------------------------------------------------------------------------------------------------*/


    /** Таблицы ---------------------------------------------------------------------------------------------------------------*/

    /**
     * Проверка присутствия значения ячейки в указанном столбце
     * @param columnName
     * @param value
     * @throws InterruptedException
     */
    @Step("Поиск присутствия значения {1} в столбце {0}")
    public Boolean checkPresenceValueInColumn(String columnName, String value) throws InterruptedException {
        
        int columnIndex = getIndexOfColumn(null, columnName);

        try {
            searchElement(By.xpath(".//tbody/tr/td[" + columnIndex + "]//a[text()='" + value + "']"));
        } catch (Exception e) {
            System.out.println("Значение " + value + " столбца " + columnName + " не найдено!");
            return false;
        }

        System.out.println("Значение " + value + " столбца " + columnName + " найдено!");
        return true;
    }

    @Step("Проверка присутствия значения {1} в столбце {0} по относительному xpath")
    public Boolean checkPresenceValueInColumn(String parentXpath, String columnName, String value) throws InterruptedException {

        try {
            getRowByColumnValue(parentXpath, columnName, value);
        } catch (Exception e) {
            System.out.println("Значение " + value + " столбца " + columnName + " не найдено!");
            return false;
        }

        System.out.println("Значение " + value + " столбца " + columnName + " найдено!");
        return true;
    }

    @Step("Получить строку по содержимому ячейки")
    public WebElement getRowByColumnValue(String parentXpath, String columnName, String value) throws InterruptedException {
        if(parentXpath == null)
            parentXpath = ".";

        int columnPosition = getIndexOfColumn(null, columnName);
        return driver.findElement(By.xpath(parentXpath + "//table/tbody/tr[td[" + columnPosition + "]//*[text()='" + value + "']]"));
    }

    @Step("Получение порядкового номера ячейки в таблице")
    public int getIndexOfColumn(String parentXpath, String columnName) {
        if(parentXpath == null)
                parentXpath = ".";

        int countOfCells = driver.findElements(By.xpath(parentXpath + "//table/thead/tr/th[span]")).size();

        for ( int i = 1; i <= countOfCells; i++ ) {
           if(driver.findElement(By.xpath(parentXpath + "//table/thead/tr/th[" + i + "]/span")).getAttribute("innerHTML").equals(columnName))
               return i;
        }

        return -1;
    }

    @Step("Получение кол-ва строк в таблице")
    public int getCountOfRows(String parentXpath) {
        if(parentXpath == null)
            parentXpath = ".";

       return driver.findElements(By.xpath(parentXpath + "//table/tbody/tr")).size();
    }
    

    /** Сортировка ---------------------------------------------------------------------------------------------------------------*/



}
