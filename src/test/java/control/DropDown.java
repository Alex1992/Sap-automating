package control;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by Александр on 22.02.2017.
 */
public class DropDown extends Control {

    protected String UIName;
    protected String xpath;
    protected WebDriver driver;

    public DropDown(WebDriver driver) {
        super(driver);
    }

    public DropDown(WebDriver driver, String UIName) {
        super(driver);
        this.UIName = UIName;
    }

    public WebElement selectItem() {
        return driver.findElement(By.xpath(".//button[@title='" + this.UIName + "']"));
    }


}
