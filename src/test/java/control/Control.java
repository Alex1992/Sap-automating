package control;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by Александр on 22.02.2017.
 */
public abstract class Control {

    protected final WebDriver driver;

    protected WebElement searchElement(By locator) {
        System.out.println(" Try to find element " + locator.toString());
        try {
            (new WebDriverWait(driver, 30L)).until(ExpectedConditions.presenceOfElementLocated(locator));
        } catch (NoSuchMethodError e) {
            System.out.println(" There is no element " + locator.toString());
            Assert.fail("Элемент " + locator.toString() + " не найден!");
        }
        return driver.findElement(locator);
    }

    protected WebElement searchElement(String xpath) {
        System.out.println(" Try to find element by xpath " + xpath);
        try {
            (new WebDriverWait(driver, 30L)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
        } catch (NoSuchMethodError e) {
            System.out.println(" There is no element by xpath " + xpath);
            Assert.fail("Элемент " + xpath + " не найден!");
        }
        return driver.findElement(By.xpath(xpath));
    }

    /**
     * Возвращает эл-т по абсолютному xpath
     * @param xpath
     * @return
     * @throws InterruptedException
     */
    public WebElement getElemByAbsXPath(String xpath) throws InterruptedException {
        Thread.sleep(250);
        return searchElement(xpath);
    }

    protected Control(WebDriver driver) {
        this.driver = driver;
    }
}
